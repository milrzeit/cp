
build server - (same as dev server for now)

note: present vs. future
  present: 1 repo per site
  future: mono repo
    - future build step: run vuejs builds instead of checking min files into git

pre-req:
  - determine which files are currently needed on server but not in git
    - assets/{a,b,c}
    - .../config/*.yaml (located outside DocumentRoot so probably N/A)
    - .htaccess files ??

Trigger build on post receive hook
  - clone repo to 'build' folder
    - TBD: use tags, then name build after tag ( maybe just do this in prod...via gui deploy/promote )
  - delete any non deployable files (*.vue, package.json, *.lock, etc, )
  - zip remaining and save/move zip file to drop-folder for future prod deploy
    - the zip drop folder will be outside of 
    - save metadata about the 'build' - file and commit hash ( mvp can save this as a flat file or part of filename )
  - (optional) do any misc testing and sanity checks we can think of
  - do the symlink dance
    - do TBD to setup this new dir to point to 'assets' 
    - switch symlink for DocumentRoot to make this site live / current
    - TBD


notes:
 - app user will own deployed files in app ... no more dev users as file owner


/stage

/current 

/releases/git-tag